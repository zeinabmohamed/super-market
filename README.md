
Android Super Market Mobile App|
===================================

This application sample of Super market with basic functionality (view products and their details ) .

Programing Language: 
- Android 
- Kotlin

Pre-requisites
--------------

- Android SDK 25
- Android Build Tools v25.0.3
- Android Support Repository
- kotlin plugin

Screenshots
-------------
![splash](screenshots/splash.png)
![product_list](screenshots/product_list.png)
![product_details](screenshots/product_details.png)

Getting Started
---------------

This App uses the Gradle build system. To build this project, use the
"gradlew build" command or use "Import Project" in Android Studio.

Dependencies
-------
- 'org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version'

- 'com.android.support:appcompat-v7:25.3.1'
- 'com.android.support:support-v4:25.3.1'   
- 'com.android.support:design:25.3.1'
- 'com.android.support.constraint:constraint-layout:1.0.2'
- 'com.android.support:recyclerview-v7:25.3.1'
- 'com.android.support:cardview-v7:25.3.1'
- 'com.squareup.retrofit2:retrofit:2.2.0'
- 'com.squareup.retrofit2:converter-gson:2.2.0'
- 'com.squareup.picasso:picasso:2.5.2'
- 'com.google.code.gson:gson:2.8.0'

- 'com.android.support.test.espresso:espresso-core:2.2.2'
- 'com.android.support.test.espresso:espresso-contrib:2.2.2'
- 'com.squareup.okhttp3:mockwebserver:3.8.1'
- 'junit:junit:4.12'
   
License
-------

Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
