/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.util

import android.content.Context

import com.zm.org.supermarket.R


/**
 * [[ErrorUtil]] handle all error messages
 */

object ErrorUtil {


    val NO_INTERNET_CONNECTION = 121
    val CONNECTION_ERROR = 122
    val SYSTEM_ERROR = 123
    val SERVER_ERROR = 124


    fun getMessage(context: Context, errorCode: Int): String {
        return when (errorCode) {
            NO_INTERNET_CONNECTION -> context.getString(R.string.check_internet_connection)

            CONNECTION_ERROR, SERVER_ERROR, SYSTEM_ERROR -> context.getString(R.string.server_not_available)

            else -> context.getString(R.string.server_not_available)
        }
    }


}
