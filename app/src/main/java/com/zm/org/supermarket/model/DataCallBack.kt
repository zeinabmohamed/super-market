/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model

import android.support.annotation.Nullable


/**
 * Common response
 */
interface DataCallBack<T> {

    fun success(response: T)

    fun error(isDialog: Boolean, @Nullable message: String)


}
