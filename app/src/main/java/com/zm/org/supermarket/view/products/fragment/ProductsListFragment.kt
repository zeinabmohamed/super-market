/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.products.fragment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zm.org.supermarket.R
import com.zm.org.supermarket.model.products.dto.Product
import com.zm.org.supermarket.model.products.dto.ProductCategory
import com.zm.org.supermarket.presenter.products.ProductsPresenter
import com.zm.org.supermarket.view.Navigator
import com.zm.org.supermarket.view.base.fragment.BaseFragment
import com.zm.org.supermarket.view.products.activity.HomeActivity
import com.zm.org.supermarket.view.products.adapter.ProductRecyclerViewAdapter
import com.zm.org.supermarket.view.products.listener.ProductsListListener
import com.zm.org.supermarket.view.products.view_model.CategoryViewModel
import kotlinx.android.synthetic.main.fragment_products_list.*


/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [com.zm.org.supermarket.view.products.fragment.OnListFragmentInteractionListener ]
 * interface.
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class ProductsListFragment : BaseFragment(), OnListFragmentInteractionListener, ProductsListListener {

    private var productsPresenter: ProductsPresenter = ProductsPresenter(this)
    private var adapter: ProductRecyclerViewAdapter? = null
    private var mColumnCount = 1
    private val ITEMS: MutableList<Any> = arrayListOf()


    companion object {

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment [ProductsListFragment].
         */
        fun newInstance(): ProductsListFragment {
            return ProductsListFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_products_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        productsToolbar.title = getString(R.string.products_list)
        (activity as AppCompatActivity).setSupportActionBar(productsToolbar)

        // Set the adapter
        if (mColumnCount <= 1) {
            productsRecyclerView.layoutManager = LinearLayoutManager(context())
        } else {
            productsRecyclerView.layoutManager = GridLayoutManager(context(), mColumnCount)
        }
        adapter = ProductRecyclerViewAdapter(context(), ITEMS, this)
        productsRecyclerView.adapter = adapter


        if (ITEMS.size <= 0) {
            productsPresenter.getProducts()
        }
    }

    override fun onListFragmentInteraction(item: Product) {

        Navigator.loadFragment(activity, ProductDetailsFragment.newInstance(item), (activity as HomeActivity).containerId, true)

    }

    override fun showProducts(response: List<ProductCategory>) {
        var colors = resources.getIntArray(R.array.rainbow)

        response.forEach {
            var index = response.indexOf(it)
            var colorResourceId = (index % colors.size)
            var categoryViewModel = CategoryViewModel(it.id, it.name, it.description, colors[colorResourceId])
            ITEMS.add(categoryViewModel)
            if (it.products != null)
                ITEMS.addAll(it.products!!)
        }

        adapter?.notifyDataSetChanged()
    }
}

/**
 * listener in menu item actions .
 */
interface OnListFragmentInteractionListener {
    fun onListFragmentInteraction(item: Product)
}

