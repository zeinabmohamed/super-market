/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.presenter.products

import com.zm.org.supermarket.model.DataCallBack
import com.zm.org.supermarket.model.products.ProductsRepo
import com.zm.org.supermarket.model.products.dto.ProductCategory
import com.zm.org.supermarket.presenter.base.BasePresenter
import com.zm.org.supermarket.view.products.listener.ProductsListListener


/**
 * handle all  business login related to [[com.zm.org.supermarket.view.products.fragment.ProductsListFragment]]
 */

class ProductsPresenter(view: ProductsListListener) : BasePresenter<ProductsListListener>(view) {

    private val productsRepo = ProductsRepo()


    fun getProducts() {
        view.showLoading()
        productsRepo.getProducts(object : DataCallBack<List<ProductCategory>> {

            override fun success(response: List<ProductCategory>) {
                view.showProducts(response)
                view.hideLoading()
            }

            override fun error(isDialog: Boolean, message: String) {
                view.hideLoading()
                if (isDialog)
                    view.showError(message)
                else
                    view.showRetry(message)
            }

        })
    }

}
