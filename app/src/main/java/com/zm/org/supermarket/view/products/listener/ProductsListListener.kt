/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.products.listener

import com.zm.org.supermarket.model.products.dto.ProductCategory
import com.zm.org.supermarket.view.base.listener.LoadDataView

interface ProductsListListener : LoadDataView {

    fun showProducts(response: List<ProductCategory>)
}