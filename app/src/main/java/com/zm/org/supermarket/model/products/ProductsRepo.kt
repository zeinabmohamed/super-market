/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.products


import com.zm.org.supermarket.model.DataCallBack
import com.zm.org.supermarket.model.base.cloud.ApiConnection
import com.zm.org.supermarket.model.base.cloud.BaseRepo
import com.zm.org.supermarket.model.products.dto.ProductCategory

/**
 * Created by Zeinab Mohamed on 3/25/2017.
 */

class ProductsRepo : BaseRepo() {

    fun getProducts(apiCallBack: DataCallBack<List<ProductCategory>>) {
        ApiConnection.retrofit()?.create(ProductsApiServices::class.java)?.products?.let { execute(it, apiCallBack) }
    }
}
