/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.products.fragment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zm.org.supermarket.AppConstants
import com.zm.org.supermarket.R
import com.zm.org.supermarket.model.base.cloud.ApiConnection
import com.zm.org.supermarket.model.products.dto.Product
import com.zm.org.supermarket.view.base.fragment.BaseFragment
import com.zm.org.supermarket.view.util.ImageUtil
import kotlinx.android.synthetic.main.fragment_product_details.*


/**
 * A fragment representing a Product details.
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class ProductDetailsFragment : BaseFragment() {

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment [ProductDetailsFragment].
         */
        fun newInstance(selectedProduct: Product): ProductDetailsFragment {
            var args = Bundle()
            var fragment = ProductDetailsFragment()
            args.putSerializable(AppConstants.PRODUCT_ITEM_KEY, selectedProduct)
            fragment.arguments = args
            return fragment
        }
    }

    private var selectedProduct: Product? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            selectedProduct = arguments.getSerializable(AppConstants.PRODUCT_ITEM_KEY) as Product
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_product_details, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        productDetailsToolbar.title = selectedProduct?.name
        (activity as AppCompatActivity).setSupportActionBar(productDetailsToolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        productDetailsToolbar.setNavigationOnClickListener {
            activity.onBackPressed()
        }

        ImageUtil.loadImage(activity, ApiConnection.BASE_URL + selectedProduct?.url!!, productImageView, 0, 0)
        productPrice.text = selectedProduct?.salePrice?.amount + selectedProduct?.salePrice?.currency
    }


}



