/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.base.activity

import android.os.Bundle
import android.os.Handler
import com.zm.org.supermarket.R
import com.zm.org.supermarket.view.Navigator


/**
 * Splash screen is the app entry point
 */
class SplashActivity : BaseActivity() {

    /**
     * default splash delay time  sec
     */
    private val SPLASH_SCREEN_DELAY_TIME: Long = 5000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            Navigator.goToHomeActivity(this@SplashActivity)
            this@SplashActivity.finish()
        }, SPLASH_SCREEN_DELAY_TIME)
    }


}
