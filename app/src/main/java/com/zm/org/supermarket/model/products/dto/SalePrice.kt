/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.products.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by zeinabmohamed on 8/12/17.
 */

class SalePrice : Serializable {
    /**
     * amount : 2.01
     * currency : EUR
     */

    @SerializedName("amount")
    var amount: String? = null
    @SerializedName("currency")
    var currency: String? = null
}