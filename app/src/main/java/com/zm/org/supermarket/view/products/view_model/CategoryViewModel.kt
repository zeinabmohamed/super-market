/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.products.view_model

class CategoryViewModel(var id: String?, var name: String?, var description: String?, var color: Int)