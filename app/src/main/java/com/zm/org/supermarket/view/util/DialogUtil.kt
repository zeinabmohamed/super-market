/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.util

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast

import com.zm.org.supermarket.R


/**
 * Dialog util
 */
object DialogUtil {


    fun getLoadingDialog(context: Context): ProgressDialog {

        val dialog = ProgressDialog(context)

        dialog.setMessage(context.getString(R.string.loading))
        dialog.setCancelable(false)
        return dialog
    }


    fun getSnackBar(context: Context, view: View, message: String,
                    onActionClick: View.OnClickListener) {

        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction(context.getString(R.string.try_again), onActionClick)
                .setActionTextColor(Color.YELLOW)
                .show()

    }

    fun getToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}
