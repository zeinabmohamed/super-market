/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.products.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.zm.org.supermarket.R
import com.zm.org.supermarket.model.base.cloud.ApiConnection
import com.zm.org.supermarket.model.products.dto.Product
import com.zm.org.supermarket.view.products.fragment.OnListFragmentInteractionListener
import com.zm.org.supermarket.view.products.view_model.CategoryViewModel
import com.zm.org.supermarket.view.util.ImageUtil

/**
 * [RecyclerView.Adapter] that can display a [Product] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class ProductRecyclerViewAdapter(private val context: Context, private val mValues: MutableList<Any>, private val mListener:
OnListFragmentInteractionListener?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            ItemType.CATEGORY.value -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.row_category_item, parent, false)
                return CategoryViewHolder(view)
            }

            ItemType.PRODUCT.value -> {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.row_product_item, parent, false)
                return ProductViewHolder(view)
            }
        }
        // default
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_product_item, parent, false)
        return ProductViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {

        when {
            mValues[position] is CategoryViewModel -> {
                return ItemType.CATEGORY.value

            }
            mValues[position] is Product -> {
                return ItemType.PRODUCT.value
            }
        }
        return ItemType.PRODUCT.value
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is CategoryViewHolder -> {
                holder.mItem = mValues[position] as CategoryViewModel

                holder.categoryNameTextView.text = holder.mItem?.name

                holder.categoryContainerFrameLayout.setBackgroundColor(holder.mItem?.color!!)

            }

            is ProductViewHolder -> {
                holder.mItem = mValues[position] as Product

                holder.productNameTextView.text = holder.mItem?.name
                ImageUtil.loadImage(context, ApiConnection.BASE_URL + holder.mItem?.url, holder.productImageView, 0, 0)

                holder.itemView.setOnClickListener {
                    mListener?.onListFragmentInteraction(holder.mItem!!)
                }

            }


        }


    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class CategoryViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val categoryNameTextView: TextView = mView.findViewById(R.id.categoryNameTextView) as TextView
        val categoryContainerFrameLayout: FrameLayout = mView.findViewById(R.id.categoryContainerFrameLayout) as FrameLayout
        var mItem: CategoryViewModel? = null


    }


    inner class ProductViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val productNameTextView: TextView = mView.findViewById(R.id.productNameTextView) as TextView
        val productImageView: ImageView = mView.findViewById(R.id.productImageView) as ImageView
        var mItem: Product? = null

    }
}


enum class ItemType(var value: Int) {
    CATEGORY(0),
    PRODUCT(1),

}
