/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view

import android.app.Activity
import android.content.Intent
import android.support.v4.app.FragmentActivity

import com.zm.org.supermarket.view.base.fragment.BaseFragment
import com.zm.org.supermarket.view.products.activity.HomeActivity

/**
 * Handle all navigation between screens from here
 */

object Navigator {

    fun loadFragment(activity: FragmentActivity,
                     baseFragment: BaseFragment, containerId: Int, isStacked: Boolean) {

        if (!isStacked) {
            activity.supportFragmentManager
                    .beginTransaction().replace(containerId,
                    baseFragment).commit()
        } else {
            activity.supportFragmentManager.beginTransaction().replace(containerId,
                    baseFragment).addToBackStack(baseFragment.javaClass.name).commit()
        }
    }

    fun goToHomeActivity(activity: Activity) {
        activity.startActivity(Intent(activity, HomeActivity::class.java))
        activity.finish()
    }
}
