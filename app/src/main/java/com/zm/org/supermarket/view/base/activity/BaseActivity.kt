/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.base.activity

import android.support.v7.app.AppCompatActivity

/**
 * base activity for all common handling.
 */
abstract class BaseActivity : AppCompatActivity()
