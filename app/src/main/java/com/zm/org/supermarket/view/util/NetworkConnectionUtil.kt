/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.util

import android.content.Context
import android.net.ConnectivityManager


/**
 * [NetworkConnectionUtil]] check all connection stuff
 */
object NetworkConnectionUtil {

    fun isNetworkAvailable(context: Context): Boolean {

        var connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var networkInfo = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }
}
