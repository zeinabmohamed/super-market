/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.base.cloud


import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Provide retrofit object to setup network connection
 */
object ApiConnection {


    private var retrofit: Retrofit? = null

    val BASE_URL: String = "http://mobcategories.s3-website-eu-west-1.amazonaws.com"

    /**
     * @return retrofit object used to create the requests
     */
    fun retrofit(): Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClient.Builder().build())
                    .build()
        }
        return retrofit
    }


}
