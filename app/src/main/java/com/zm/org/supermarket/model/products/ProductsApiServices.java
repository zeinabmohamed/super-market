/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.products;

import com.zm.org.supermarket.model.products.dto.ProductCategory;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Zeinab Mohamed on 3/17/2017.
 */

public interface ProductsApiServices {


    @GET("/")
    Call<List<ProductCategory>> getProducts();


}
