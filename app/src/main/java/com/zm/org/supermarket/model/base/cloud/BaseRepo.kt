/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.base.cloud

import com.zm.org.supermarket.AppApplication
import com.zm.org.supermarket.model.DataCallBack
import com.zm.org.supermarket.view.util.ErrorUtil
import com.zm.org.supermarket.view.util.NetworkConnectionUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * handle all cloud service in from common point
 */
abstract class BaseRepo {


    protected fun <T> execute(call: Call<T>, dataCallBack: DataCallBack<T>) {

        if (!NetworkConnectionUtil.isNetworkAvailable(AppApplication.context!!)) {
            dataCallBack.error(false,
                    ErrorUtil.getMessage(AppApplication.context!!,
                            ErrorUtil.NO_INTERNET_CONNECTION))
        } else {
            call.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    val data = response.body()

                    if (response.isSuccessful && data != null) {

                        dataCallBack.success(data)
                    } else {
                        dataCallBack.error(false,
                                ErrorUtil.getMessage(AppApplication.context!!,
                                        ErrorUtil.CONNECTION_ERROR))
                    }
                }

                override fun onFailure(call: Call<T>, throwable: Throwable) {
                    dataCallBack.error(false,
                            ErrorUtil.getMessage(AppApplication.context!!,
                                    ErrorUtil.CONNECTION_ERROR))
                }
            })
        }

    }


}
