/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.products.dto

import com.google.gson.annotations.SerializedName

/**
 * Created by zeinabmohamed on 8/12/17.
 */

class ProductCategory {

    /**
     * id : 36802
     * name : Food
     * description :
     * products : [{"id":"1","categoryId":"36802","name":"Bread","url":"/Bread.jpg","description":"","salePrice":{"amount":"0.81","currency":"EUR"}},{"id":"2","categoryId":"36802","name":"Sandwich","url":"/Sandwich.jpg","description":"","salePrice":{"amount":"2.01","currency":"EUR"}},{"id":"3","categoryId":"36802","name":"Milk","url":"/Milk.jpg","description":"","salePrice":{"amount":"2.00","currency":"EUR"}},{"id":"4","categoryId":"36802","name":"Cake","url":"/Cake.jpg","description":"","salePrice":{"amount":"0.01","currency":"EUR"}}]
     */

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("products")
    var products: List<Product>? = null


}
