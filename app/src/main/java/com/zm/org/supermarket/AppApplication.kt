/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket

import android.app.Application
import android.content.Context

/**
 * [AppApplication] to add all configurations steps needed
 */

class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object {

        var context: Context? = null
            private set
    }


}
