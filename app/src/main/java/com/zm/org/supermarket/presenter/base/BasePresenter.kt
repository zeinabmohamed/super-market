/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.presenter.base

import com.zm.org.supermarket.view.base.listener.LoadDataView

/**
 */
abstract class BasePresenter<out T : LoadDataView>(protected val view: T)
