/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.products.activity

import android.os.Bundle

import com.zm.org.supermarket.R
import com.zm.org.supermarket.view.Navigator
import com.zm.org.supermarket.view.base.activity.BaseActivity
import com.zm.org.supermarket.view.products.fragment.ProductsListFragment

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        Navigator.loadFragment(this, ProductsListFragment.newInstance(), containerId, false)

    }

    val containerId: Int
        get() = R.id.homeContainer
}
