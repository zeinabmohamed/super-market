/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.base.fragment

import android.app.ProgressDialog
import android.content.Context
import android.support.v4.app.Fragment
import com.zm.org.supermarket.view.base.listener.LoadDataView
import com.zm.org.supermarket.view.util.DialogUtil


/**
 * BaseFragment for all common handling
 */
abstract class BaseFragment : Fragment(), LoadDataView {


    private var progressDialog: ProgressDialog? = null


    override fun context(): Context = activity

    override fun showLoading() {
        if (progressDialog == null)
            progressDialog = DialogUtil.getLoadingDialog(context())

        progressDialog!!.show()
    }

    override fun hideLoading() {
        progressDialog!!.dismiss()

    }


}
