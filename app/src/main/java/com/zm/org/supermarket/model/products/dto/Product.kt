/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.model.products.dto

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by zeinabmohamed on 8/12/17.
 */

class Product : Serializable {

    /**
     * id : 2
     * categoryId : 36802
     * name : Sandwich
     * url : /Sandwich.jpg
     * description :
     * salePrice : {"amount":"2.01","currency":"EUR"}
     */

    @SerializedName("id")
    var id: String? = null
    @SerializedName("categoryId")
    var categoryId: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("url")
    var url: String? = null
    @SerializedName("description")
    var description: String? = null
    @SerializedName("salePrice")
    var salePrice: SalePrice? = null


}
