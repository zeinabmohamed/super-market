/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.base.listener

import android.content.Context
import com.zm.org.supermarket.view.util.DialogUtil

/**
 * Interface representing a View that will use to load data.
 * adding default handling for methods
 */
interface LoadDataView {


    /**
     * Show a view with a progress bar indicating a loading process.
     */
    fun showLoading()

    /**
     * Hide a loading view.
     */
    fun hideLoading()

    /**
     * Show a retry view in case of an error when retrieving data.

     * @param message
     */
    fun showRetry(message: String) {
        DialogUtil.getToast(context(), message)
    }

    /**
     * Hide a retry view shown if there was an error when retrieving data.
     */
    fun hideRetry() {

    }

    /**
     * Show an error message

     * @param message A string representing an error.
     */
    fun showError(message: String) {
        DialogUtil.getToast(context(), message)
    }

    /**
     * Get a [Context].
     */
    fun context(): Context

}
