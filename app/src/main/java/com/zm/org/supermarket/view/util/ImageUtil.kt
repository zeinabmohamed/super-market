/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket.view.util


import android.content.Context
import android.widget.ImageView
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import com.zm.org.supermarket.R
import okhttp3.OkHttpClient


/**
 * Created by abdelmaw on 12/5/2016.
 */

object ImageUtil {


    fun loadImage(context: Context, imageUrl: String,
                  targetImageView: ImageView, placeHolderImageId: Int, errorImageId: Int) {

        var client = OkHttpClient.Builder().build()
        Picasso.Builder(context)
                .downloader(OkHttp3Downloader(client))
                .build()
                .load(imageUrl)
                .placeholder(when (placeHolderImageId > 0) {
                    true -> placeHolderImageId
                    false -> R.drawable.placeholder
                })
                .error(when (errorImageId > 0) {
                    true -> errorImageId
                    false -> R.drawable.placeholder
                })
                .fit()
                .into(targetImageView)
    }


}
