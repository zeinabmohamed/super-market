/*
 * Copyright (c) 2017.  All Code CopyRights reserved For Zeinab Mohamed Abdelmawla
 *
 */

package com.zm.org.supermarket;

import com.zm.org.supermarket.model.products.ProductsApiServices;
import com.zm.org.supermarket.model.products.dto.ProductCategory;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by zeinabmohamed on 8/17/17.
 */

public class IntegrationWithServerTest {
    @Test
    public void getProducts() throws IOException {
        MockWebServer mockWebServer = new MockWebServer();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Set a response for retrofit to handle. You can copy a sample
        //response from your server to simulate a correct result or an error.
        //MockResponse can also be customized with different parameters
        //to match your test needs
        mockWebServer.enqueue(new MockResponse().setBody("[\n" +
                "  {\n" +
                "    \"id\": \"36802\",\n" +
                "    \"name\": \"Food\",\n" +
                "    \"description\": \"\",\n" +
                "    \"products\": [\n" +
                "      {\n" +
                "        \"id\": \"1\",\n" +
                "        \"categoryId\": \"36802\",\n" +
                "        \"name\": \"Bread\",\n" +
                "        \"url\": \"/Bread.jpg\",\n" +
                "        \"description\": \"\",\n" +
                "        \"salePrice\": {\n" +
                "          \"amount\": \"0.81\",\n" +
                "          \"currency\": \"EUR\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"2\",\n" +
                "        \"categoryId\": \"36802\",\n" +
                "        \"name\": \"Sandwich\",\n" +
                "        \"url\": \"/Sandwich.jpg\",\n" +
                "        \"description\": \"\",\n" +
                "        \"salePrice\": {\n" +
                "          \"amount\": \"2.01\",\n" +
                "          \"currency\": \"EUR\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"3\",\n" +
                "        \"categoryId\": \"36802\",\n" +
                "        \"name\": \"Milk\",\n" +
                "        \"url\": \"/Milk.jpg\",\n" +
                "        \"description\": \"\",\n" +
                "        \"salePrice\": {\n" +
                "          \"amount\": \"2.00\",\n" +
                "          \"currency\": \"EUR\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"id\": \"4\",\n" +
                "        \"categoryId\": \"36802\",\n" +
                "        \"name\": \"Cake\",\n" +
                "        \"url\": \"/Cake.jpg\",\n" +
                "        \"description\": \"\",\n" +
                "        \"salePrice\": {\n" +
                "          \"amount\": \"0.01\",\n" +
                "          \"currency\": \"EUR\"\n" +
                "        }\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "]"));

        ProductsApiServices service = retrofit.create(ProductsApiServices.class);

        //With your service created you can now call its method that should
        //consume the MockResponse above. You can then use the desired
        //assertion to check if the result is as expected. For example:
        Call<List<ProductCategory>> call = service.getProducts();
        Response<List<ProductCategory>> response = call.execute();
        assertTrue(response != null); // check not null

        assertTrue(response.body().get(0) != null && response.body().get(0) instanceof ProductCategory);  //check parsing correct
        assertEquals("Food", response.body().get(0).getName()); //  check one of the values correct after values correct

        //Finish web server
        mockWebServer.shutdown();
    }
}
